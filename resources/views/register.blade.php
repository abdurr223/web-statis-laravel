<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
            integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N"
            crossorigin="anonymous"
        />
        <title>Sign Up Form</title>
    </head>
    <body>
        <div class="container">
            <div class="offset-md-3 col-md-6">
            <div class="mt-5">
                <h2 class="text-center">Sign Up Form</h2>
            </div>
            <form action="/welcome" method="get">
                <div>
                    <label for="firstName" class="form-label"
                        >First name:</label
                    >
                    <input type="text" id="firstName" class="form-control" />
                </div>
                <div>
                    <label for="lastName" class="form-label">Last name:</label>
                    <input type="text" id="lastName" class="form-control" />
                </div>
                <div class="mt-4 d-flex">
                    <label for="Gender">Gender</label>
                    <div class="radio ml-2">
                        <input type="radio" id="male" name="gender" />
                        <label for="male">Male</label>
                    </div>
                    <div class="radio ml-2">
                        <input type="radio" id="female" name="gender" />
                        <label for="female">Female</label>
                    </div>
                    <div class="radio ml-2">
                        <input type="radio" id="other" name="gender" />
                        <label for="other">Other</label>
                    </div>
                </div>
                <div>
                    <label for="" class="form-label">Nationality:</label>
                    <select
                        id="Nationality"
                        name="Nationality"
                        class="form-control"
                    >
                        <option value="ind">Indonesia</option>
                        <option value="mly">Malaysia</option>
                        <option value="singa">Singapore</option>
                        <option value="Bru">Brunei</option>
                    </select>
                </div>
                <div class="mt-3">
                    <label for="" class="form-label">Language Spoken:</label>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="indo" />
                        <label for="indo">Bahasa Indonesia</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="eng" />
                        <label for="eng">English</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="other1" />
                        <label for="other1">Other</label>
                    </div>
                </div>
                <div>
                    <label for="" class="form-label">Bio:</label>
                    <textarea
                        name=""
                        id=""
                        cols="30"
                        rows="10"
                        class="form-control"
                        style="resize: none"
                    ></textarea>
                </div>
                <div>
                    <button type="submit" class="btn btn-success mt-3">Sign Up</button>
                </div>
            </form>
        </div>
        </div>
        <script
            src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
            crossorigin="anonymous"
        ></script>
    </body>
</html>
